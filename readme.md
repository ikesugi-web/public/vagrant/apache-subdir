# 開発環境構築

`git clone https://`

## 前提

* Vagrantの最新版をインストールしていること
* Virtualboxの最新版をインストールしていること
* Gitをインストールしていること

## 構築手順

1. `vagrant-vbguest`プラグインのインストール
  `vagrant plugin install vagrant-vbguest`
1. Vagrantfileの編集
必須以外はやらなくてもよい
  * 【必須】git_idとgit_passに自分のIDとパスワードを入力する
  * イントラネットにローカル環境を公開する場合は以下のコメントアウトを外す
  ```
  config.vm.network "public_network", ip: public_ip
  ```
  ※ipを固定しない場合はipオプションを削除する
  ※固定する場合は`public_ip`に空いているIPを入力する

  * フォルダ同期のnfs設定
    * NFSを使うことでVagrant環境を高速化することが可能
    * 以下のコメントアウトを外して`type: "virtualbox"`の方をコメントアウトする
    ```
    config.vm.synced_folder ".", "/vagrant", type:"nfs"
    #config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
    ```
    
    * Windows環境の場合はNFSを利用するには`vagrant-winnfsd`というプラグインが必要
    `vagrant plugin install vagrant-winnfsd`

1. Vagrantを起動する
  Vagrantfileの配置ディレクトリで以下を実行
  `vagrant up`


## 接続確認手順

1. hostsファイルに以下を設定する
  `プライベートIP nama.local`

1. ブラウザで以下へアクセス
  * frontend：`http://nama.local/`
  * api：`http://nama.local/api/`

## 各作業ディレクトリパス

* Frontend(Vue)
  `docker/frontend`
* API(Laravel)
  `docker/api`

## 各操作について

* Vagrantへ接続する
  ホストマシンから以下にて接続する
  `vagrant ssh`
* 作業ディレクトリへ移動する
  `cd /vagrant`

* コンテナの起動
作業ディレクトリで以下を実行
`docker-compose up -d`

* コンテナの停止
`docker-compose stop`

* 各コンテナの状態確認
`docker ps`

* 各コンテナへ入る
作業ディレクトリで各コマンドを実行
  * frontend
    `docker exec -it frontend ash`
  * api
    `docker exec -it api ash`

* Frontend
* API
  * composerの実行
    1. apiコンテナに入る
      `docker exec -it api ash`
    1. composerを実行する
      `composer install`等

