#!/bin/sh

# Disabled SELinux
setenforce 0
sed -i.org -e "/^SELINUX=/s/enforcing/disabled/" /etc/selinux/config

# epel repo
yum -y install http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-ExtUtils-MakeMaker wget autoconf


# Git
cd /home/vagrant
wget https://github.com/git/git/archive/v2.24.0.tar.gz
tar zxvf v2.24.0.tar.gz
rm -rf v2.24.0.tar.gz
cd git-2.24.0/
make configure
./configure --prefix=/usr/local
make all
make install

# root��PATH��ʂ�
sed -e '/Defaults    secure_path = \/sbin:\/bin:\/usr\/sbin:\/usr\/bin/s/$/:\/usr\/local\/bin/' /etc/sudoers | EDITOR=tee visudo >/dev/null

